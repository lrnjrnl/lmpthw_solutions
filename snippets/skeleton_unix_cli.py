#!/usr/bin/env python3

import argparse, sys

# parse command options
parser = argparse.ArgumentParser()
parser.add_argument('FILES', nargs='*')

args = parser.parse_args()
    
# assign input data to list
def get_data():
    global args
    data_in = []
    # data from either pipe or FILES
    if not sys.stdin.isatty():
        data_in = [line for line in sys.stdin]
    elif args.FILES:
        for file in args.FILES:
            with open(file) as fin:
                data_in.extend(fin.readlines())
    else:
        print('specify input')
            
    return data_in
    
# send output data to stdout
def give_output(pro_data):
    for line in pro_data:
        print(line, end='')

give_output(get_data())