# This is a demonstration of a class attribute implementation in Python 3
class Parent(object):
    x = 0

mother = Parent()
father = Parent()

print("""
class Parent(object):
    x = 0

mother = Pather()
father = Pather()""")

print("\nParent.x: ", Parent.x)
print("mother.x:", mother.x)
print("father.x", father.x)

Parent.x += 1
print("\nParent.x += 1")
print("\nParent.x: ", Parent.x)
print("mother.x:", mother.x)
print("father.x", father.x)

mother.x += 1
print("\nmother.x += 1")
print("Parent.x: ", Parent.x)
print("mother.x: ", mother.x)
print("father.x: " , father.x)
