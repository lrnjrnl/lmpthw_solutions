In 2018, I spent time working through computer science concepts in Python using Zed Shaw's book, Learn More Python the Hard Way.

This is a collection of my solutions to the programming exercises. They include hand-built data structures, algorithms and process-hacking projects (making command line utils in python). I even got to design and build a BEDMAS math expression parser from scratch.

The most challenging part of the course was where I had to implement a stripped-down Python interpreter. It is constructed from a scanner/tokenizer, parser, semantic analyzer and interpreter and which features lexical scope, function definitions and function calls. I wrote the parser and grammars by hand because I hadn't yet learned about parser-generators. The code I wrote for this section of the book is under directories ex32 to ex37. One of these days I'll get around to packaging this thing up to make it easier demo and play with.

My development notes can be read in a public thread on Zed's help forum: https://forum.learncodethehardway.com/t/thinking-out-loud/1374/18