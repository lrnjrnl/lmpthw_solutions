#!/usr/bin/env python3
import scanner

CODE = """5 LET S = 0
10 MAT INPUT V
20 LET N = NUM
30 IF N = 0 THEN 99
40 FOR I = 1 TO N
45 LET S = S + V(I)
50 NEXT I
60 PRINT S/N
70 GO TO 5
99 END"""
# figure out how to regex match strings enclosed by quotes
PATTERNS = [
            ("^\n+\s*", "NEWLINE"),
            ("^END\s*", "END"),
            ("^[0-9]+\s*", "NUMBER"),
            # investigate pattern for numbers in scientific notation
            ("^(([0-9]+\.[0-9]*)|([0-9]*\.[0-9]+))\s*", "NUMBER"),
            ("^LET\s*", "LET"),
            ("^\,\s*", "COMMA"),
            ("^\(\s*", "LPAREN"),
            ("^\)\s*", "RPAREN"),
            ("^DEF\s*", "DEF"),
            ("^FN[A-Z]\s*", "FNID"),
            ("^PRINT\s*", "PRINT"),
            ("^GO\s*TO\s*", "GOTO"),
            ("^GOSUB\s*", "GOSUB"),
            ("^RETURN\s*", "RETURN"),
            ("^IF\s*", "IF"),
            ("^THEN\s*", "THEN"),
            ("^MAT\s*", "MAT"),
            ("^INPUT\s*", "INPUT"),
            ("^FOR\s*", "FOR"),
            ("^TO\s*", "TO"),
            ("^STEP\s*", "STEP"),
            ("^NEXT\s*", "NEXT"),
            ("^\=\s*", "EQUAL"),
            ("^\<\s*", "LSTHAN"),
            ("^\>\s*", "GRTHAN"),
            ("^\<\=\s*", "LSEQTO"),
            ("^\>\=\s*", "GREQTO"),
            ("^\<\>\s*", "NOTEQ"),
            ("^\+\s*", "SUM"),
            ("^\-\s*", "DIF"),
            ("^\/\s*", "QUO"),
            ("^\*\s*", "PRO"),
            ("^\^\s*", "POW"),
            ("^([A-Z][0-9]?)\s*", "VARID"),

           ]

if __name__=="__main__":
    scanny = scanner.Scanner(PATTERNS, CODE)
    for t in scanny.tokens: print(t)
