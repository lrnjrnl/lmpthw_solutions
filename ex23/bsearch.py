def bsearch(pylist, value, lo=None, hi=None):
    # print("len(pylist): ", len(pylist))
    # print("value: ", value)
    if hi == None:
        lo = 0
        hi = len(pylist)
    middle = ((hi - lo) // 2) + lo
    # print("middle: " , middle)
    if pylist[middle] == value:
        # print(":::case1")
        return middle
    elif  value < pylist[middle]:
        # print(">>>case2")
        return bsearch(pylist, value, lo, middle)
    elif value > pylist[middle]:
        # print("***case3")
        return bsearch(pylist, value, middle, hi)