from dllist import *

# Implememt bubble, merge and quick sort algorithms.
'''
def bubble_sort(numbers):
    """Sorts a list of numbers using bubble sort."""
    while True:
        # start off assuming it's sorted
        is_sorted = True
        # comparing 2 at a time, skipping ahead
        node = numbers.begin.nxt
        while node:
            # loop through comparing node to the next
            if node.prev.value > node.value:
                node.prev.value, node.value = node.value, node.prev.value
                # oops, looks like we have to scan again
                is_sorted = False
            node = node.next
        
        # this is reset at the top but if we never swapped then it's sorted
        if is_sorted: break
'''
    
def bubba_srot(nums):
    """sort element values in ascending order"""
    while True:
        ordered = True
        n = nums.begin
        n1 = n.nxt
        while n1:
            if n.value > n1.value:
                n.value, n1.value = n1.value, n.value
                ordered = False
            n = n1
            n1 = n1.nxt
        if ordered: break
    
    
    
    
'''My tests for bubba_srot
reverse_srot = DoubleLinkedList()
reverse_srot.push(5)
reverse_srot.push(4)
reverse_srot.push(3)
reverse_srot.push(2)
reverse_srot.push(1)

bubba_srot(reverse_srot)

print("reverse_srot:")
for i in range(0, 5):
    print(f"reverse_srot({i})", reverse_srot.get(i))

already_srot = DoubleLinkedList()
already_srot.push(1)
already_srot.push(2)
already_srot.push(3)
already_srot.push(4)
already_srot.push(5)

bubba_srot(already_srot)

print("\n>>>already_srot:")
for i in range(0, 5):
    print(f"already_srot({i})", already_srot.get(i))

dupes = DoubleLinkedList()
dupes.push(1)
dupes.push(3)
dupes.push(5)
dupes.push(1)
dupes.push(3)

bubba_srot(dupes)

print("\n>>>dupes:")
for i in range(0, 5):
    print(f"dupes({i})", dupes.get(i))

randomes = DoubleLinkedList()
randomes.push(4)
randomes.push(1)
randomes.push(5)
randomes.push(3)
randomes.push(2)

bubba_srot(randomes)

print("\n>>>randomes:")
for i in range(0, 5):
    print(f"randomes({i})", randomes.get(i))
'''