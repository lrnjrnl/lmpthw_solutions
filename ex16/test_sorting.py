import sorting
from dllist import DoubleLinkedList
from random import randint

max_numbers = 30

def random_list(count):
    numbers = DoubleLinkedList()
    for i in range(count, 0, -1):
        numbers.shift(randint(0, 10000))
    return numbers
    
    
def is_sorted(numbers):
    node = numbers.begin
    while node and node.nxt:
        if node.value > node.nxt.value:
            return False
        else:
            node = node.nxt
        
    return True
    
    
def test_bubba_srot():
    numbers = random_list(max_numbers)
        
    sorting.bubba_srot(numbers)
        
    assert is_sorted(numbers)
        
        
def test_merge_sort():
    pass