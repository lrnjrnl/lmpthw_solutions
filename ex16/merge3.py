from dllist import *
def merge_sort(A, B, n):
    """A contains the data, B is for rearranging"""
    copy_list(A, 0, n, B)
    split_merge(B, 0, n, A)
    
def split_merge(B, iBegin, iEnd, A):
    if iEnd - iBegin < 2:
        return
    iMiddle = ((iBegin + iEnd) // 2)
    # left side
    split_merge(A, iBegin, iMiddle, B)
    # right_side
    split_merge(A, iMiddle, iEnd, B)
    merge(B, iBegin, iMiddle, iEnd, A)
    
def merge(A, iBegin, iMiddle, iEnd, B):
    i = iBegin
    j = iMiddle
    k = iBegin
    b_node = B.begin
    while k <= iEnd:
        print("A.get(i)", A.get(i))
        print("A.get(j)", A.get(j))
        if i < iMiddle and (j >= iEnd or A.get(i) <= A.get(j)):
            print("mergeLS", A.get(i))
            # b_node.value = A.get(i)
            i += 1
        else:
            print("mergeRS", A.get(j))
            # b_node.value = A.get(j)
            j += 1
        # b_node = b_node.nxt
        k = k +1
    
def copy_list(A, iBegin, iEnd, B):
    k = iBegin
    while k < iEnd:
        B.push(A.get(k))
        k += 1

def moso(A):
    B = DoubleLinkedList()
    merge_sort(A, B, A.count())
   

# VVVV TESTING VVVV
h = DoubleLinkedList()
h.push(12)
h.push(10)
h.push(13)
h.push(1)
h.push(11)

for i in range(0, h.count()):
    print(f"h.get({i}): {h.get(i)}")
    
moso(h)
print("    >>>moso: ")
for i in range(0, h.count()):
    print(f"h.get({i}): {h.get(i)}")