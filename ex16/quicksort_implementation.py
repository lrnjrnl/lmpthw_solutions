from dllist import *
from random import shuffle

def qsrot(A):
    quicksort(A, 0, A.count() - 1)
    
def refer_node(A, idx):
    """return node object which corresponds with index parameter"""
    acount = A.count()
    if idx > acount - 1:
        return None
    i = 0
    node = A.begin
    while i < idx:
        node = node.nxt
        i += 1
    return node
    
def asn_idx(A, idx, val):
    """assign val to idx node of list A"""
    acount = A.count()
    if idx > acount - 1:
        raise f"index out of range index: {idx} max:{acount - 1}"
    i = 0
    node = A.begin
    while i < idx:
        node = node.nxt
        i += 1
    node.value = val
    return
        
    
def quicksort(A, lo, hi):
    if lo < hi:
        p = partition(A, lo, hi)
        quicksort(A, lo, p - 1)
        quicksort(A, p + 1, hi)
        
        
def partition(A, lo, hi):
    pivot = A.get(hi)
    i = lo - 1
    j = lo
    inode = None
    while j <= hi - 1:
        jnode = refer_node(A, j)
        if jnode.value < pivot:
            i += 1
            inode = refer_node(A, i)
            jnode.value, inode.value = inode.value, jnode.value
        j += 1
    iplusnode = refer_node(A, i + 1)
    hinode = refer_node(A, hi)
    if hinode.value < iplusnode.value:
        hinode.value, iplusnode.value = iplusnode.value, hinode.value
    return i + 1
            





# VVV TEST refer_node HERE VVV
h = DoubleLinkedList()
g = list(range(6, 16))
# shuffle(g)
for f in g:
    h.push(f)
    
for i in range(h.count()):
    print(f"h[{i}]: ", h.get(i))
    
qsrot(h)
print(">>>qsrot(h)")

for i in range(h.count()):
    print(f"h[{i}]: ", h.get(i))
    
# Test asn_idx
# print("asn_idx(h, 0, 'powow'")
# asn_idx(h, 0, 'powow')
# print("asn_idx(h, 6, 'smudging'")
# asn_idx(h, 6, 'smudging')
# print("asn_idx(h, count() - 1, 'longhouse'")
# asn_idx(h, h.count() - 1, 'longhouse')

# for i in range(h.count()):
#     print(f"h[{i}]: ", h.get(i))

shuffle(g)
for f in g:
    h.push(f)
    
for i in range(h.count()):
    print(f"h[{i}]: ", h.get(i))
    
qsrot(h)
print(">>>qsrot(h)")

for i in range(h.count()):
    print(f"h[{i}]: ", h.get(i))