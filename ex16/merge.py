from dllist import *

def merge_sort(A):
    alength = A.count()
    # Base case. A list of zero or one elements is sorted, by definition.
    if alength <= 1:
        return A
        
    # Recursive case. First, divide the list into equal-sized sublists
    # consisting of the first half and second half of the list.
    # This assumes lists start at index 0.
    left = DoubleLinkedList()
    right = DoubleLinkedList()
    halfa = alength // 2
    index = 0
    while A.begin:
        if index < halfa:
            left.push(A.unshift())
        else:
            right.push(A.unshift())
        index +=1
    
    # Recursively sort both sides
    L = merge_sort(left)
    R = merge_sort(right)
    
    # Then merge the now-sorted sublists
    return merge(L, R)
    
def merge(left, right):
    result = DoubleLinkedList()
    
    while left.begin and right.begin:
        if left.first() <= right.first():
            result.push(left.unshift())
        else:
            result.push(right.unshift())
            
        # Either left or right may have elements left; consume them
        # print("left.begin", left.begin)
        # print("right.begin", right.begin)
        # (Only one of the following loops will actually be entered)
    while left.begin:
        print("Lvalue", left.begin.value)
        result.push(left.unshift())
    while right.begin:
        print("Rvalue", right.begin.value)
        result.push(right.unshift())
            
    return result