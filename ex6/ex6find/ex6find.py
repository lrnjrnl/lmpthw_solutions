import argparse, glob, re

parser=argparse.ArgumentParser()
parser.add_argument("path", const=1, nargs='?', default="./")
parser.add_argument("-name")
# parser.add_argument("-type", action="store_true")

args = parser.parse_args()

for elt in glob.iglob(args.path + '**', recursive=True):
    print('>>>', args.name, elt)
    if args.name in elt:
        print(elt)

