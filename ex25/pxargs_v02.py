#!/usr/bin/env python3

import subprocess
import sys

args = sys.argv[1:]

if len(args) == 0 or "--help" == args[0]:
    print("Usage: [source > stdin] | python pxargs.py [command] [arguments]")
    exit(0)

options = { "-I":None}
# parse options from arguments, but stop once subprocess command is reached
# increment flags i+=1
# increment positional args i+=2
i = 0
while i < len(args):
    if args[i] == "-I":
        options["-I"] = args[i+1]
        i += 2
    else:
        break
# collect subcommand and its options
subp_args = args[i:]

if subp_args[0] != options["-I"]:
    subp_args.insert(0, "/bin/echo")

# keep track of which arguments are to be substituted with line from stdin.
_I_indices = []

for i in range(len(subp_args)):
    if subp_args[i] == options["-I"]:
        _I_indices.append(i)
        
pipe_in = [arg.strip() for arg in sys.stdin.readlines()]

for line in pipe_in:
    if options["-I"]:
        for i in _I_indices:
            subp_args[i] = line
    
    subprocess.run(subp_args)
    
# recombine above for full command
