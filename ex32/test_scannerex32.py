import scannerex32

TOKENS = [
            (r"^def",                       "DEF"),
            (r"^[a-zA-Z_][a-zA-Z0-9_]*",     "NAME"),
            (r"^\(",                        "LPAREN"),
            (r"^[0-9]+",                     "INTEGER"),
            (r"^\+",                        "PLUS"),
            (r"^\)",                        "RPAREN"),
            (r"^:",                         "COLON"),
            (r"^\s+",                       "INDENT"),
            (r"^,",                         "COMMA"),
            ]
            
            
CODE = [
"def hello(x, y):",
"    print(x + y)",
"hello(10, 20)",
]

def test_scanner():
    global TOKENS
    global CODE
    lilscan = scannerex32.Scanner(CODE, TOKENS)
    script = lilscan.scan()
    i = 0
    for token in script:
        print(i, token)
        i += 1
    print("peek('NAME'):", lilscan.peek('NAME'))
    assert lilscan.peek('NAME') == [2, 4, 7, 11, 13, 17, 19]
    first_token = lilscan.match_script('DEF')
    assert first_token.lexeme == 'def'
    assert lilscan.script[0].token == 'INDENT'
    lilscan.push(first_token)
    assert lilscan.script[0].token == 'DEF'

        
if __name__=="__main__":
    test_scanner()