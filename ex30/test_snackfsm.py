import snackfsm

def test_dispenser():
    dispenser = snackfsm.snackFSM()
    test_events = ['coin', 'code_incorrect', 'code_correct','shake']
    for event in test_events:
        dispenser.handler(event)
        
if __name__=="__main__":
    test_dispenser()