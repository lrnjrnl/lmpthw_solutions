import fsm
        
class snackFSM(fsm.FSM):
    def __init__(self):
        fsm.FSM.__init__(self)
        self.state_name = 'ATTRACT'
        
    def START(self):
        self.state_name = 'ATTRACT'
            
    def ATTRACT(self, event):
        print('Insert coin')
        if event == 'coin':
            return 'MENU'
        else:
            return 'ATTRACT'
            
    def MENU(self, event):
        print('Menu of snacks...')
        if event == 'code_correct':
            print('Dispensing!')
            return 'ATTRACT'
        elif event == 'code_incorrect':
            print('Invalid snack ID')
            return 'MENU'
        elif event == 'coin':
            print('Returning coin')
            return 'MENU'
        else:
            return 'ERROR'
            
    def ERROR(self, event):
        print('ERROR', event)
        return 'ERROR'
        
