class DoubleLinkedListNode(object):
    
    def __init__(self, value, nxt, prev):
        self.value = value
        self.nxt = nxt
        self.prev = prev
        
        
    def __repr__(self):
        nval = self.nxt and self.nxt.value or None
        pval = self.prev and self.prev.value or None
        return f"[node:{self.value}, next:{repr(nval)}, prev:{repr(pval)}]"
        

class DoubleLinkedList(object):
    
    def __init__(self):
        self.begin = None
        self.end = None
        self.tally = 0

    def push(self, obj):
        """Appends a new value on the end of the list."""
        if self.begin == None:
            push_node = DoubleLinkedListNode(obj, None, None)
            self.begin = push_node
            self.end = self.begin
        else:
            push_node = DoubleLinkedListNode(obj, None, self.end)
            self.end.nxt = push_node
            self.end = self.end.nxt
        self.tally += 1
    
    def pop(self):
        """Removes the last item and returns it."""
        pop_val = None
        if self.end == None:
            return pop_val
        elif self.end.prev == None:
            pop_val = self.end.value
            self.end = None
            self.begin = None
            return pop_val
        else:
            pop_val = self.end.value
            self.end = self.end.prev
            return pop_val
        self.tally -= 1
    
    def shift(self, obj):
        """Actually just another name for push."""
        if self.begin == None:
            shift_node = DoubleLinkedListNode(obj, None, None)
            self.begin = shift_node
            self.end = self.begin
        else:
            shift_node = DoubleLinkedListNode(obj, self.begin, None)
            self.begin.prev = shift_node
            self.begin = shift_node
        self.tally += 1
        
    def unshift(self):
        """Removes the first item (from begin) and returns it."""
        unshift_val = None
        if self.begin:
            unshift_val = self.begin.value
            self.tally -= 1
            if self.begin.nxt:
                self.detach_node(self.begin)
            else:
                self.detach_node(self.begin)
                # self.end = self.begin
        return unshift_val
        
    def detach_node(self, obj):
        """You'll need to use this operation sometimes, but mostly
        inside remove(). It should take a node, and detach it from the list, whether the the node is at the front, end or in the middle."""
        assert self.begin != None
        assert self.end != None
        if self.begin == self.end:
            self.begin = None
            self.end = self.begin
        elif obj == self.begin:
            self.begin = self.begin.nxt
        elif obj == self.end:
            self.end = self.end.prev
        else:
            prev_node = obj.prev
            nxt_node = obj.nxt
            prev_node.nxt = nxt_node
            nxt_node.prev = prev_node
        
        
    def remove(self, obj):
        """Finds a matching item and removes it from the list."""
        itr = self.begin
        index = 0
        while itr:
            if itr.value == obj:
                self.detach_node(itr)
                self.tally += 1
                return index
            itr = itr.nxt
            index += 1
        return None

    def first(self):
        """Returns a *reference* to the first item, does not remove."""
        if self.begin:
            return self.begin.value
        else:
            return None
    
    def last(self):
        """Returns a reference to the last item, does not remove."""
        if self.end:
            return self.end.value
        else:
            return None
            
    def count(self):
        """Count the number of elements in the list."""
        # Performance: This gets called too often to generate
        # on the fly. Store value attribute and update in the
        # "add/remove element" methods? Or, store an index
        # within each node, with count() returning
        # index + 1 for the self.end node
        return self.tally
        
        
    def get(self, index):
        """Get the value at index."""
        if index > self.count() - 1:
            return None
        get_node = self.begin
        count = 0
        while get_node:
            if count > index:
                return None
            elif count == index:
                return get_node.value
            else:
                get_node = get_node.nxt
                count += 1
                
    def insort(self, value):
        # print(f"111BEGIN: {self.begin} VALUE: {value} END: {self.end} \n"); input()
        self.tally += 1
        if self.begin == None:
            # print(f"222BEGIN: {self.begin} VALUE: {value} END: {self.end} \n"); input()
            self.begin = DoubleLinkedListNode(value, None, None)
            self.end = self.begin
            return
        else:
            node = self.begin
            
        while node:
            if value >= node.value:
                node = node.nxt
            else:
                break
        if node == None:
            self.end.nxt = DoubleLinkedListNode(value, None, self.end)
            self.end = self.end.nxt
        else:
            if not node.prev:
                node.prev = DoubleLinkedListNode(value, node, None)
                self.begin = node.prev
            else:
                new_node = DoubleLinkedListNode(value, node, node.prev)
                node.prev.nxt = new_node
                node.prev = new_node
        return
        
                    
            
    def bsearch(self, value, lo=None, hi=None):
        """ binary search algorithm """
        if hi == None:
            lo = 0
            hi = self.count()
        while hi > lo:
            middle = ((hi - lo) // 2) + lo
            mid_val = self.get(middle)
            # print(f"value = {value}, d[{middle}] = {mid_val}")
            if mid_val == value:
                # print(":::case1")
                return middle
            elif  value < mid_val:
                # print(">>>case2")
                hi = middle
            elif value > mid_val:
                # print("***case3")
                lo = middle
        return None
            
        
        
    def dump(self, mark):
        """Debugging function that dumps the contents of the list."""
        print(mark, [self.get(i) for i in range(self.tally)])
            
    def _invariant(self):
        if self.begin == None:
            assert self.end == None
        if (self.begin or self.end):
            assert self.begin.prev == None
            assert self.end.nxt == None
            
            
if __name__=="__main__":
    d = DoubleLinkedList()
    d.insort('bbb')
    d.insort('aaa')
    d.insort('aab')
    d.insort('abb')
    d.dump("DDList")