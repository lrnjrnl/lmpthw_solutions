class BSTreeNode(object):
    
    def __init__(self, key, value, parent=None, left=None, right=None):
        self.key = key
        self.value = value
        self.parent = parent
        self.left = left
        self.right = right
    
    def __repr__(self):
        return f"[{self.key} : {self.value}]"
        
class BSTree(object):
    
    def __init__(self):
        self.root = None
        
    def set(self, key, value, node=None):
        if node == None:
            if self.root:
                node = self.root
            else:
                self.root = BSTreeNode(key, value)
                return
            
        if str(key) == str(node.key):
            node.value = value
        elif str(key) < str(node.key):
            if node.left:
                node = node.left
                self.set(key, value, node)
            else:
                node.left = BSTreeNode(key, value, node)
        elif str(key) > str(node.key):
            if node.right:
                node = node.right
                self.set(key, value, node)
            else:
                node.right = BSTreeNode(key, value, node)
    '''
    def insert_flat(self, key, value, node=None):
    force the BSTree to take the shape of a linked list.
        if node == None:
            node = self.root
        if str(key) == str(node.key):
            node.value = value
        elif str(key) < str(node.key):
            if node.left:
                node = node.left
                self_insert.flat(key, value, node)
            else:
                node.left = node
        elif str(key) > str(node.key):
            if node.parent:
                node = node.parent
                self_insert.flat(key, value, node)
    '''
            
                
    def get(self, key, node=None):
        if node == None:
            if self.root:
                node = self.root
            else:
                return None
        if key == node.key:
            return node.value
        elif str(key) < str(node.key):
            return node.left and self.get(key, node.left)
        elif str(key) > str(node.key):
            return node.right and self.get(key, node.right)
        return None
    
    def list(self):
        print(self.list_recurse(self.root))
    
    def list_recurse(self, node):
        if node:
            left_side = self.list_recurse(node.left)
            right_side = self.list_recurse(node.right)
            return f"{left_side} \n {node.key}:{node.value} \n {right_side}"
        else:
            return ''
    
    def refer_node(self, key, node=None):
        """ find node by key and return node object"""
        if node == None:
            if self.root:
                node = self.root
            else:
                return None
        if key == node.key:
            return node
        elif str(key) < str(node.key):
            return node.left and self.refer_node(key, node.left)
        elif str(key) > str(node.key):
            return node.right and self.refer_node(key, node.right)
        return None
        
    def find_minimum(self, node):
        """ use this for deleting node with 2 children"""
        return node and self.find_minimum(node.left) or node
    
    def move_successors(self, node):
        """ MORE TESTS: in delete case2"""
        if node == None:
            return
        successor = node and self.find_minimum(node.right) or None
        print("\n ***SUCCESSOR***", successor, "***NODE***", node)
        if successor:
            node.key, node.value = successor.key, successor.value
            if successor.right:
                self.move_successors(successor.right)
            else:
                successor.parent.left = None
        
    def delete(self, key):
        dnode = self.refer_node(key)
        print(f"delete({key})")
        # case1: no matching key
        if dnode == None:
            print(">>>case1")
            return None
        # make a references
        pnode = dnode.parent
        # case2: both children
        if dnode.left and dnode.right:
            print(">>>case2")
            # Successor handling
            self.move_successors(dnode)
        # case3: no children
        elif not (dnode.left or dnode.right):
            print(">>>case3")
            # no parent? must be self.root
            if not pnode:
                self.root = None
            elif dnode == pnode.left:
                pnode.left = None
            elif dnode == pnode.right:
                pnode.right = None
        # case4: one child (single_branch)
        elif (dnode.left and not dnode.right) or (dnode.right and not dnode.left):
            print(">>>case4")
            single_branch = dnode.left or dnode.right
            # no parent? must be root
            if not pnode:
                self.root = single_branch
                self.root.parent = None
            else:
                if dnode == pnode.left:
                    pnode.left, single_branch.parent = single_branch, pnode
                elif dnode == pnode.right:
                    pnode.right, single_branch.parent = single_branch, pnode

                
if __name__=="__main__":
    g = BSTree()
    g.set('Michigan', 'MI')
    g.set('Florida', 'FL')
    g.set('Oregon', 'OR')
    g.set('Alaska', 'AK')
    g.set('Minnesota', 'MN')
    g.set('Wyoming', 'WY')
    g.set('Pennsylvania', 'PA')
    g.set('Hawaii', 'HI')
    g.set('Alabama', 'AL')
    g.set('Conneticut', 'CT')
    g.set('Kentucky', 'KY')
    g.list()
    print('delete Alabama, Conneticut')
    g.delete('Alabama')
    g.delete('Conneticut')
    g.list()
    print('set Alabama, Conneticut')
    g.set('Alabama', 'AL')
    g.set('Conneticut', 'CT')
    g.list()
    print('>>>delete Michigan')
    g.delete('Michigan')
    g.list()
    print('>>>set Michigan')
    g.set('Michigan', 'MI')
    g.list()
    # print('>>>delete Hawaii')
    # g.delete('Hawaii')
    # g.list()
    # print('>>>set Hawaii')
    # g.set('Hawaii', 'HI')
    # g.list()
    # print('>>>delete Kentucky')
    # g.delete('Kentucky')
    # g.list()
    # print('>>>delete Kentucky')
    # g.delete('Kentucky')
    # g.list()