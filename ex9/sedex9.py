import sys, re

script_name = sys.argv.pop(0)
action_arg = sys.argv.pop(0)
files = sys.argv[:]

# parse the sub 's' action
action_list = action_arg.split('/')
action_list.pop()
action = action_list.pop(0)
pattern = action_list.pop(0)
repl = action_list.pop(0)

# compile the pattern and perform substitution on each line of each file
pat = re.compile(pattern)

for file in files:
    fin = open(file)
    for line in fin.readlines():
        # print(fin.read(), end='')
        print(re.sub(pat, repl, line), end='')
    fin.close()