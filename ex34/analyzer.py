import parser
import productions

class World(object):
    def __init__(self, variables):
        self.functions = {}
        self.variables = variables

class Analyzer(object):
    # get the parse tree root and run its visit method
    # to recurse through the tree
    def __init__(self, parse_tree, world):
        self.world = world
        self.parse_tree = parse_tree
        
    
    def analyze(self):
        self.parse_tree[0].visit(self.world)
        
        
if __name__=="__main__":
    pass