#!/usr/bin/env python3
import scanner
import parser
from productions import *
import analyzer
import pprint



TOKENS = [
            (r"^def",                       "DEF"),
            (r"^[a-zA-Z_][a-zA-Z0-9_]*",    "NAME"),
            (r"^\(",                        "LPAREN"),
            (r"^[0-9]+",                    "INTEGER"),
            (r"^\+",                        "PLUS"),
            (r"^\)",                        "RPAREN"),
            (r"^:",                         "COLON"),
            (r"^\s\s\s\s",                  "INDENT"),
            (r"^\s+",                       "WHITESPACE"),
            (r"^,",                         "COMMA"),
            ]
            
            
CODE = """def hello(x, y):
    print(x + y)
    
hello(10, 20)

hello(4, 7)
"""

def test_Scanner():
    global TOKENS
    global CODE
    lilscan = scanner.Scanner(TOKENS, CODE)
    print(lilscan.tokens)

def test_Parser():
    global TOKENS
    global CODE
    # print(parser)
    lilparse = parser.Parser(TOKENS, CODE)
    print(lilparse.parse_tree)

def test_Analyzer():
    variables = {}
    world = analyzer.World(variables)
    script = [
        Root([
        FuncDef("print",
                ArgsFormal([
                        ArgF('printarg'),
                        ]),
                Body([
                   ])
                ),
        FuncDef("hello",
                ArgsFormal([
                        ArgF('x'), ArgF('y')
                        ]),
                Body([
                    FuncCall("print",
                            Params([
                                ExprPlus(ExprVariable('x'), ExprVariable('y'))
                                ])
                            )
                    ])),
        FuncCall("hello",
                Params([ExprNumber(10), ExprNumber(20)])
                )
         ])]
    pynalyze = analyzer.Analyzer(script, world)
    pynalyze.analyze()
    print('>>>\tscript: ', script)
    print('>>>\tvariables: ', pynalyze.world.variables)
    print('>>>\tfunctions:', pynalyze.world.functions)
    
if __name__=="__main__":
    test_Analyzer()
    
# ExprPlus(ExprVariable('x'), ExprVariable('y'))
