import scanner
import productions

# This is my third attempt at the scanner-parser from ex32-33. This time,
# the NewLine class tracks empty lines, newlines and indent/dedent.
# No whitespace or indent tokens are passed to the parser.

# I think it's possible that the indent tracking can be confined to the
# body() method since that's the only point at which indents should be changing
# (although have a look at funcdef())
# Example condition:
    # self.peek_indent() == indent_level
# This could be a sanity check, but it could also signal the upcoming end of a
# block/body
# eg.
# def block(self)
    # self.indent_level += 1
    # while self.peek_indent() == self.indent_level:
    #     block_contents.append(stuff)
    # self.indent_level -= 1
    # return productions.Block(block_contents)

class Parser(object):
    def __init__(self, token_list, code):
        self.scanner = scanner.Scanner(token_list, code)
        self.indent_level = -1
        self.productions = self.parse()
        self.root = self.productions[0]
        
    def peek_indent(self):
        return self.scanner.peek_indent()

    def peek(self):
        return self.scanner.peek()
        
    def get_code(self):
        return self.scanner.get_code()
        
    def match(self, token_id):
        return self.scanner.match(token_id)
        
    def push(self, token):
        self.scanner.push(token)
        
    def parse(self):
        return [self.root()]
        
    def compare_indent(self):
        """ Upcoming dedent returns False. indent asserts error """
        # if self.peek_indent() > self.indent_level:
        #         assert False, f"Unexpected indent at {self.get_code()}"
        # print('PEEK ', self.peek_indent(), ' LEVEL ', self.indent_level)
        # print('PEEK', self.peek())
        if self.scanner.tokens and self.peek_indent() >= self.indent_level:
            return True
        else:
            return False
    
        
    ### GRAMMARS
    
    def root(self):
        """ root = *(funccall / funcdef) """
        root_body = self.body()
        return productions.Root(root_body)
        
    def funccall(self):
        """ funccall = NAME LPAREN params RPAREN """
        name = self.match('NAME')
        self.match('LPAREN')
        params = self.params()
        self.match('RPAREN')
        return productions.FuncCall(name, params)
        
    def funcdef(self):
        """ funcdef = DEF NAME LPAREN params RPAREN COLON body """
        self.match('DEF')
        name = self.match('NAME')
        self.match('LPAREN')
        args_formal = self.args_formal()
        self.match('RPAREN')
        self.match('COLON')
        assert self.peek_indent() == self.indent_level + 1
        body = self.body()
        return productions.FuncDef(name, args_formal, body)
    
    def params(self):
        """ params = expression *(COMMA expression) """
        param_contents = []
        
        while self.peek() != 'RPAREN':
            assert self.peek() in ('NAME', 'INTEGER')
            param_contents.append(self.expression())
            if self.peek() == 'COMMA':
                self.match('COMMA')
        return productions.Params(param_contents)
    
        
    # for funcdef
    # analyzer will register FormArgs
    def args_formal(self):
        """ args_formal = arg_formal *(COMMA arg_formal) """
        formal_contents = []
        while self.peek() != 'RPAREN':
            assert self.peek() == 'NAME'
            formal_contents.append(productions.argf())
            if self.peek() == 'COMMA':
                self.match('COMMA')
        return productions.args_formal(formal_contents)
            
    def argf(self):
        """ returns a formal argument """
        assert self.peek() == 'NAME'
        name_token = self.match('NAME')
        return productions.ArgF(name_token)
        
    def body(self):
        """ body = *(funcdef / expression) """
        self.indent_level += 1
        body_contents = []
        # only keep building body contents if indent_level stays the same
        # this condition is broken!
        while self.compare_indent():
            token_id = self.peek()
            if token_id == 'DEF':
                body_contents.append(self.funcdef())
            # elif token_id == 'NAME':
            #     name = self.match('NAME')
            #     token_id = self.peek
            #     self.push(name)
            #     if token_id == 'LPAREN':
            #         body_contents.append(self.funccall())
            #     elif self.compare_indent():
            #         body_contents.append(self.expression())
            else:
                body_contents.append(self.expression())
        result = productions.Body(body_contents, self.indent_level)
        self.indent_level -= 1
        return result
        
    def expression(self):
        """ expression = funccall / variable / integer / plus"""
        token_id = self.peek()
        if token_id == 'NAME':
            token = self.match('NAME')
            token_id = self.peek()
            if token_id == 'LPAREN':
                self.push(token)
                left = productions.ExprFCall(self.funccall())
            else:
                left = productions.ExprVariable(token)
        elif token_id == 'INTEGER':
            token = self.match('INTEGER')
            left = productions.ExprNumber(token)
        token_id = self.peek()
        if token_id == 'PLUS':
            self.match('PLUS')
            return self.plus(left)
        else:
            return left
            
    def plus(self, left):
        """ plus = expression PLUS expression """
        right = self.expression()
        return productions.ExprPlus(left, right)
        