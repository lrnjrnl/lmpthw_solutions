#!/usr/bin/env python3
import sys
#
# INPUT PROCESSING
#
def take_bytes(dump_in, offset):
    """grab 16 or remaining bytes starting from offset and calculuate new offset"""
    remaining = len(dump_in) - offset
    if remaining > 16:
        line = dump_in[offset:offset + 16]
        new_offset = offset + 16
    else:
        line = dump_in[offset:offset + remaining]
        new_offset = offset + remaining
    return line, new_offset


#
# DISPLAY FUNCTIONS
#
def white_dot(line):
    """copy string, replace all whitespace with '.', and return copy"""
    line_out = line[:]
    for c in ('\n'):
        line_out = line_out.replace(c, '.')
    return line_out
    
def default_display(line_in, offset):
    line_out = line_in
    return line_out

def canonical(line_in, offset):
    offset_display = hex(offset)[2:].zfill(8)
    line = line_in[:]
    i = 0
    line_out = offset_display + '  '
    while i < len(line):
        line_out += hex(ord(line[i]))[2:].zfill(2)
        i += 1
        if i < 16:
            line_out += ' '
        if i == 8:
            line_out += ' '
    # append sixteen bytes in %_p format enclosed in '|' chars
    line_out += ((60 - len(line_out)) * ' ') + '|' + white_dot(line) + '|\n'
    return line_out

def twobyte_dec(line_in, offset):
    line_out = line_in
    return line_out
    
def onebyte_oct(line_in, offset):
    line = line_in[:]
    offset_display = hex(offset)[2:].zfill(7)
    i = 0
    line_out = offset_display + ' '
    while i < len(line):
        line_out += oct(ord(line[i]))[2:].zfill(3)
        i += 1
        if i < 16:
            line_out += ' '
    line_out += ((71 - len(line_out)) * ' ') + '\n'
    return line_out
    
def onebyte_char(line_in, offset):
    line = line_in[:]
    offset_display = hex(offset)[2:].zfill(7)
    i = 0
    line_out = offset_display + '   '
    while i < len(line):
        line_out += line[i]
        i += 1
        if i < 16:
            line_out += '   '

    line_out += ((71 - len(line_out)) * ' ') + '\n'
    return line_out
    
def twobyte_oct(line_in, offset):
    line_out = line_in
    return line_out

#
# OUTPUT PROCESSING
#
def process_offset_display(offset):
    """The final offset display must have the same number of place values
    as the last display option in the command arguments"""
    if displays[-1] == '-C':
        offset_display = hex(offset)[2:].zfill(8)
    else:
        offset_display = hex(offset)[2:].zfill(7)
    return offset_display

def process_byte_display(line_in, offset):
    """apply all displays to input"""
    output = ""
    for display in displays:
        output += display_funcs[display](line_in, offset)
    return output
        

#
# PARSING
# no support for format strings
#
display_funcs = {
                "-C" : canonical,
                "-d" : twobyte_dec,
                "-b" : onebyte_oct,
                "-c" : onebyte_char,
                "-o" : twobyte_oct,
                "--default-display" : default_display
                }
                
flags = {}

args = sys.argv[1:]
if args and (args[0] == "--help" or args[0] == "-h"):
    print("...put usage instruction here...")
    exit(0)


# output displays may be rendered multiple times per input line
# hence the list
displays = []
files = []
feed = ""

i = 0
while i < len(args):
    if args[i] in display_funcs.keys():
        displays.append(args[i])
        i += 1
    elif args[i] in flags.keys():
        flags[args[i]] = True
        i += 1
    else:
        # handle bad argument
        files = args[i:]
        break
        
if not displays:
    displays.append("--default-display")

#
# collect feeds from input sources (files or stdin)
#
if files:
    for file in files:
        with open(file) as fin:
            feed += fin.read()
else:
    feed = sys.stdin.read()

#
# process input!
#
offset = 0
dump_out = ''
#input_offset_display = offset
while offset < len(feed):
    # render input offset as zero-padded, 10-char hex display
    # grab 16 bytes (or remaining) and reposition offset
    line_in, new_offset = take_bytes(feed, offset)
    dump_out += process_byte_display(line_in, offset)
    offset = new_offset
dump_out += process_offset_display(offset)
print(dump_out)


