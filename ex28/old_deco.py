import math

print("\n1. function within a function\n")

def papa(x):
    print(f"This is the papa {x}")
    def mama(y):
        return x + y + 1
    print("calling the mama")
    return mama(5)
    
print(papa(3))
print(papa(4))


print("\n2. returning a function\n")

def weasel(x):
    print(f"weasel is a slippery {x}")
    def ferret(y):
        print(f"feeling ferrety for {x} * {y} days!")
        return x * y
    return ferret
    
times_one = weasel(1)
times_two = weasel(2)
print("3 times_one(): ", times_one(3))
print("3 times_two(): ", times_two(3))

print("\n3. passing function as parameter\n")
def drill():
    print("Drill: 'grrrr grrrr grrr'")

def forklift():
    print("Forklift: 'vweee vweee vwee'")

def tractor(tool):
    print(f"Worker: 'I've got a {tool.__name__} on my tractor!'")
    tool()

print()
print(f"Foreman: 'Get me a forklift!'")
tractor(forklift)

print()
print(f"Foreman: 'Get me a drill!'")
tractor(drill)

# polynomial factory

def polynomial_factory(*coefficients):
    def polynomial(x):
        result = 0
        for degree, coeff in enumerate(coefficients):
            result += coeff*x**degree
        return result
    return polynomial
    
poly_func1 = polynomial_factory(0, 0, 1)

print("poly_func1(3): ", poly_func1(3))