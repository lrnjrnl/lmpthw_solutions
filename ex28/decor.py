def my_decor(func):
    def wrapper(*a, **kwargs):
        print()
        print(f"{wrapper.__name__} is calling {func.__name__}")
        res = func(a, kwargs)
        print(f"{wrapper.__name__} is finished with {func.__name__}")
        res += f"\nwrapped by {wrapper.__name__}"
        print()
        return res
    return wrapper
    

def say_xyz(*args, **kwargs):
    print("say_xyz", args, kwargs)
    return f"Thanks for the call to {say_xyz.__name__}"
    
undecorated = say_xyz
print()
print("say_xyz no wrapper ==>", say_xyz(1,2,3, blah='booyah'))
say_xyz = my_decor(say_xyz)
print()
print("say_xyz with wrapper ==>", say_xyz(4,5,6, boyo='bizabah'))
print()
print("undecorated_xyz sez ==>", undecorated(7,8,9, bibib='babab'))
