# 1 Get a first test going using mock.
# 2 Do better parsing of the command line.
# 3 Handle special commands like exit and cd
# 4 Handle ctrl-c keyboard input
# 5 Add readline if available


import subprocess
import readline
import os

def hello():
    print("Hello Ctr-D!")
    
readline.parse_and_bind('tab: complete')
readline.parse_and_bind('\C-j: exit')
readline.set_auto_history(True)
while True:
    command_prompt = input(">")
    command = command_prompt.strip().split(' ')
    if command[0] == 'cd':
        try:
            os.chdir(command[1])
        except:
            print(f"cd {command[1]}: No such file or directory")
    elif command[0] == 'exit':
        exit(0)
    else:
        try:
            subprocess.run(command)
        except:
            print(f"{command[0]} not found")