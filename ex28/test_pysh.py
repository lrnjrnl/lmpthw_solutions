from mock import patch
from pysh import *
import sys


@patch('subprocess.run')
def test_command_run(subpr):
    command = ['ls', '-la']
    command_run(command)
    assert subpr is subprocess.run
    assert subpr.called
    assert subprocess.run.called
    
@patch('os.chdir')
def test_command_cd(osdir):
    command = ['cd', '../']
    command_run(command)
    assert os.chdir is osdir
    assert os.chdir.called
    os.chdir.assert_called_with('../')
    
@patch('sys.exit')
def test_command_exit(ex):
    command = ['exit']
    command_run(command)
    assert ex is sys.exit
    assert sys.exit.called
    sys.exit.assert_called_with(0)
    
@patch('subprocess.run')
def test_parse_user(subpr):
    pass

