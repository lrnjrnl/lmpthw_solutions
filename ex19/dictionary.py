from dllist import *

class Dictionary(object):
    def __init__(self, num_buckets=256):
        self.num_buckets = num_buckets
        self.map = DoubleLinkedList()
        i = 0
        while i < num_buckets:
            self.map.push(DoubleLinkedList())
            i += 1
    
    def hash_key(self, key):
        return hash(key) % self.num_buckets
            
        
    def get_slot(self, key):
        bucket = self.get_bucket(key)
        
        if bucket:
            slot = bucket.begin
            while slot:
                if slot.value[0] == key:
                    return bucket, slot
                slot = slot.nxt
        # There should always be a bucket,
        # but just in case...
        return bucket, None
                
        
    def get_bucket(self, key):
        bucket_id = self.hash_key(key)
        return self.map.get(bucket_id)
        

    def set(self, key, value):
        bucket, slot = self.get_slot(key)
        
        if slot:
            slot.value = (key, value)
        else:
            bucket.push((key, value))
            
            
    def get(self, key):
        bucket, slot = self.get_slot(key)
        
        if slot:
            return slot.value[1]
        else:
            return None
        
    def delete(self, key):
        bucket, slot = self.get_slot(key)
        bucket.detach_node(slot)
    
    def list(self):
        bucket = self.map.begin
        while bucket:
            slot = bucket.value and bucket.value.begin or None
            while slot:
                print(f"k: {slot.value[0]}, v: {slot.value[1]}")
                slot = slot.nxt
            bucket = bucket.nxt
            
    
    
# TEST
if __name__ == '__main__':
    d = Dictionary(16)
    # print(d.get_bucket('abc'))
    d.set('blah', 'first key')
    d.set('blahblah', 'second key')
    d.set(1234, 'third key')
    # print('blah: ', d.get('blah'))
    # print('blahblah: ', d.get('blahblah'))
    # print(1234, d.get(1234))
    # print(12345, d.get(12345))
    # d.delete('blah')
    # print('blah: ', d.get('blah'))
    d.list()