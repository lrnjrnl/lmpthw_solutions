import sorting
import dictionary
from dllist import DoubleLinkedList
from random import randint

max_numbers = 1600

def random_list(count):
    numbers = DoubleLinkedList()
    for i in range(count, 0, -1):
        numbers.shift(randint(0, 10000))
    return numbers
    
    
def is_sorted(numbers):
    node = numbers.begin
    while node and node.nxt:
        if node.value > node.nxt.value:
            return False
        else:
            node = node.nxt
        
    return True
    
    
def test_bubble_sort():
    numbers = random_list(max_numbers)
    sorting.bubble_sort(numbers)
    assert is_sorted(numbers)
        
        
def test_msort():
    numbers = random_list(max_numbers)
    sorting.msort(numbers)
    assert is_sorted(numbers)

def test_qsort():
    numbers= random_list(max_numbers)
    sorting.qsort(numbers)
    assert is_sorted(numbers)

def test_dictionary():
    d = Dictionary(16)
    d.set('blah', 'first key')
    d.set('blahblah', 'second key')
    d.set(1234, 'third key')
    assert d.get('blah') == 'first key'
    assert d.get('blahblah') == 'second key'
    assert d.get(1234) == 'third key'
    assert d.get(12345) == None
    d.delete('blah')
    assert d.get('blah') == None
    d.set('blahblah', 'fourth key')
    assert d.get('blahblah') == 'fourth key'
    d.set('blah', 'fifth key')
    assert d.get('blah') == 'fifth key'
    d.list()
    
def test_DoubleLinkedList():
    d = random_list()
    assert d.count() == max_numbers
    
if __name__ == '__main__':
    test_bubble_sort()
    test_msort()
    test_qsort()