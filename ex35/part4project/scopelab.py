#!/usr/bin/env python3
# I'm testing how the enclosing environment of a function definition affects
# the scope of variable x

def recursive_caller(x):
    if x == 1:
        print("BASE CASE x ==1")
    else:
        caller_level = 4 - x
        print(f">>>ENTER caller level:{caller_level} x:{x}")
        def hello():
            print("hello x: ", x)
        hello()
        recursive_caller(x-1)
        print(f"<<<EXIT caller level:{caller_level} x:{x}")
        hello()

recursive_caller(3)
