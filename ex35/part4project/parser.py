import scanner
import productions

class Parser(object):
    def __init__(self, token_map, code):        
        self.tokenizer = scanner.Scanner(token_map)
        self.tokenizer.scan(code)
        # Abstract Syntax Tree
        self.ast = None

    def peek(self):
        return self.tokenizer.peek()

    def match(self, token_type):
        return self.tokenizer.match(token_type)
    
    def push(self, token):
        self.tokenizer.push(token)
    
    def pop(self):
        return self.tokenizer.pop()
    
    def get_indent(self):
        return self.tokenizer.get_indent()

    def get_line_num(self):
        return self.tokenizer.get_line_num()

    def parse(self):
        self.ast = self.body(root=True)

    def statement(self):
        """ *(expr | asgn_id EQUAL expr) """
        expression = None
        assign_id = None
        stmt_is_return = False
        stmt_line = self.get_line_num() 
        if self.peek() == "NAME" and self.name_is_assign():
            name_token = self.match("NAME")
            name = name_token.lexeme
            assign_id = productions.AssignID(name)
            self.match("ASSIGN")
        elif self.peek() == "RETURN":
            stmt_is_return = True
            self.match("RETURN")
        if self.get_line_num() == stmt_line:
            expression = self.expr()
        else:
            assert False, "Syntax Error, Incomplete Assignment"
        return productions.Statement(expression, assign_id, stmt_is_return)

    def name_is_assign(self):
        name_token = self.match("NAME")
        if self.peek() == "ASSIGN":
            self.push(name_token)
            return True
        else:
            self.push(name_token)
            return False

    def body(self, root=False):
        """ *(statement | func_def) """
        contents = []
        current_indent = self.get_indent()
        next_indent = current_indent
        while next_indent >= current_indent:
            if self.peek() == "DEF":
                contents.append(self.func_def())
            else:
                contents.append(self.statement())
            next_indent = self.get_indent()
        if root==True:
            return productions.Root(current_indent, contents)
        else:
            return productions.Body(current_indent, contents)
    
    def func_def(self):
        """ DEF name LPAREN formal_args RPAREN COLON body """
        self.match("DEF")
        name_token = self.match("NAME")
        name = name_token.lexeme
        self.match("LPAREN")
        formal_args = self.formal_args()
        self.match("RPAREN")
        self.match("COLON")
        body = self.body()
        return productions.FuncDef(name, formal_args, body)

    def formal_args(self):
        """ f_arg *(COMMA f_arg) """
        arguments = []
        while self.peek() != "RPAREN":
            assert self.peek() == "NAME"
            arguments.append(self.f_arg())
            if self.peek() == "COMMA":
                self.match("COMMA")
            else:
                break
        return arguments

    def f_arg(self):
        """ NAME """
        name_token = self.match("NAME")
        name = name_token.lexeme
        return productions.FArg(name)

    def actual_args(self):
        """ expr *(COMMA expr) """
        arguments = []
        while self.peek() != "RPAREN":
            arguments.append(self.expr())
            if self.peek() == "COMMA":
                self.match("COMMA")
            else:
                break
        return arguments

    def expr(self):
        """ expr_int | expr_add | expr_name | func_call """
        token_type = self.peek()
        if token_type == "INTEGER":
            left_side = self.expr_int()
        elif token_type == "NAME":
            if self.name_is_call():
                left_side = self.func_call()
            else:
                left_side = self.expr_id()
        else:
            assert False, f"INVALID EXPR TOKEN, {token_type}"
        binary_operator = self.has_bin_op()
        if binary_operator == "PLUS":
            return self.expr_plus(left_side)
        else:
            return left_side

    def name_is_call(self):
        """ check if NAME is followed by LPAREN """
        token = self.pop()
        if self.peek() == "LPAREN":
            name_is_call = True
        else:
            name_is_call = False
        self.push(token)
        return name_is_call

    def has_bin_op(self):
        """ check if operator follows current token and return operator type"""
        has_operator = self.peek()
        if has_operator == None:
            return None  
        elif has_operator in ('PLUS'):
            return has_operator 
        else:
            return None

    def expr_id(self):
        """ NAME """
        name_token = self.match("NAME")
        name = name_token.lexeme
        return productions.ExprID(name)

    def expr_int(self):
        """ INTEGER  """
        token = self.match("INTEGER")
        value = int(token.lexeme)
        return productions.ExprInt(value)    

    def expr_plus(self, left_expr):
        """ PLUS  """
        self.match("PLUS")
        right_expr = self.expr()
        return productions.ExprPlus(left_expr, right_expr)

    def func_call(self):
        """ NAME LPAREN actual_args RPAREN """
        # print(f">>> START func_call>type:{self.peek()} line:{self.get_line_num()} indent:{self.get_indent()}")
        name_token = self.match("NAME")
        name = name_token.lexeme
        self.match("LPAREN")
        actual_args = self.actual_args()
        self.match("RPAREN")
        # print(f">>> END func_call>type:{self.peek()} line:{self.get_line_num()} indent:{self.get_indent()}")
        return productions.FuncCall(name, actual_args)
