class Production(object): pass

class Statement(Production):
    def __init__(self, expression, assign_id, stmt_is_return):
        self.assign_id = None
        self.stmt_is_return = stmt_is_return
        if assign_id:
            self.assign_id = assign_id
        self.expression = expression

    def visit(self, local_scope):
        self.expression.visit(local_scope)
        if self.assign_id:
            self.assign_id.visit(local_scope)
            if self.stmt_is_return:
                assert False, "ANALYSIS ERROR: can't return an assignment"
            elif type(self.expression) == ExprID:
                v = local_scope.symbol_table[self.expression.name]
                local_scope.symbol_table[self.assign_id.name] = v
            else:
                # just an expression
                pass
               
    def run(self, call_frame):
        result = self.expression.run(call_frame)
        if self.assign_id and self.stmt_is_return:
            assert False, "RUN ERROR: can't return assignment"
        elif self.assign_id:
            call_frame.symbol_table[self.assign_id.name] = result
        elif self.stmt_is_return:
            call_frame.return_value = result
        else:
            # just an expression
            pass

    def __repr__(self):
        return f"\n<Statement>RETURN:{self.stmt_is_return} NAME:{self.assign_id}, {self.expression}\n</Statement>"

class AssignID(object):
    def __init__(self, name):
        self.name = name

    def visit(self, local_scope):
        local_scope.analyze_id(self.name) 
        

    def run(self, call_frame):
        call_frame.insert_symbol(self.name)

    def __repr__(self):
        return f"\n<AssignID>name:{self.name}\n</AssignID>"

class Expr(object): pass

class ExprInt(Expr):
    def __init__(self, value):
        self.value = value
    
    def visit(self, local_scope):
        pass

    def run(self, call_frame):
        return self

    def __repr__(self):
        return f"\n<ExprInt>value:{self.value}<\ExprInt>"

class ExprID(Expr):
    def __init__(self, name):
        self.name = name
    
    def visit(self, local_scope):
        local_scope.symbol_exists(self.name)

    def run(self, call_frame):
        return call_frame.lookup_symbol(self.name)

    def __repr__(self):
        return f"\n<ExprID>name:'{self.name}'</ExprID>"

class ExprPlus(Expr):
    def __init__(self, left, right):
        self.left_expr = left
        self.right_expr = right
    
    def visit(self, local_scope):
        self.left_expr.visit(local_scope)
        self.right_expr.visit(local_scope)

    def run(self, call_frame):
        # instantiate result with a value of 0 because it requires
        # one argument
        result = ExprInt(0)
        right = self.right_expr.run(call_frame)
        left = self.left_expr.run(call_frame)
        result.value = left.value + right.value
        return result

    def __repr__(self):
        return f"\n<ExprPlus>{self.left_expr}{self.right_expr}\n</ExprPlus>"

class FuncCall(Expr):
    def __init__(self, name, params):
        self.name = name
        self.params = params
        self.return_value = None

    def visit(self, local_scope):
        # the local_scope will lookup the name in the 
        # symbol table and verify the number of parameters
        # corresponds to the number of formal arguments
        # in the function definition
        function, scope = local_scope.lookup_symbol(self.name)
        assert len(self.params) == len(function.formal_args),\
                "function call params don't match fargs"
        for elt in self.params:
            elt.visit(local_scope)
    
    def run(self, call_frame):
        # get the (function, scope) pair
        func_and_env = call_frame.lookup_symbol(self.name)
        function, environment = func_and_env
        new_call_frame = environment.spawn_call_frame()
        # copy parameters (unevaluated, AST form) from call environment
        # into callee's scope
        for farg, aarg in zip(function.formal_args, self.params):
            new_call_frame.symbol_table[farg.name] = aarg.run(call_frame)
        # call the function and get its return value
        function.call(new_call_frame)
        call_frame.return_value = new_call_frame.return_value  

    def __repr__(self):
        return f"\n<FuncCall {self.name}>params:{self.params}\n</FuncCall>"

class FArg(object):
    def __init__(self, name):
        self.name = name 
    
    def visit(self, local_scope):
        local_scope.analyze_id(self.name)
        assert local_scope.symbol_exists(self.name)

    def run(self, call_frame):
        return self

    def __repr__(self):
        return f"\n<FArg name:'{self.name}'></FArg>"

class FuncDef(object):
    def __init__(self, name, formal_args, body):
        # name at time of definition
        self.name = name
        self.formal_args = formal_args
        self.body = body

    def visit(self, local_scope):
        # each time a function is visited (called?), it should empty/zero out
        # values of local variables.
        child_local_scope = local_scope.declare_function(self, local_scope)
        for elt in self.formal_args:
            elt.visit(child_local_scope)
        return self.body.visit(child_local_scope)

    def run(self, call_frame):
        #function definition is bundled with its enclosing call_frame/scope
        call_frame.symbol_table[self.name] = (self, call_frame)
    
    def call(self, call_frame):
        # evaluate the function
        self.body.run(call_frame)


    def __repr__(self):
        return f"\n<FuncDef name:{self.name}>{self.formal_args}{self.body}\n</FuncDef>"
        # return f"\n<FuncDef name:{self.name}></FuncDef>"

class BIPrint(FuncDef):
    """ a built-in print function at the root level """
    def __init__(self):
        self.name = 'print'
        self.formal_args = [FArg('prtarg')]

    def visit(self, local_scope, parent=None):
        child_local_scope = local_scope.declare_function(self, local_scope)
        for elt in self.formal_args:
            elt.visit(child_local_scope)
          
    def call(self, call_frame):
        expression = call_frame.symbol_table['prtarg']
        result = expression.run(call_frame)
        print(result.value)
        call_frame.return_value = None

    def __repr__(self):
        return f"\n<BIPrint>{self.formal_args}\n</BIPrint>"
        # return f"\n<BIPrint name:{self.name}<\BIPrint>"

class Body(object):
    """ *(expr | funcdef) """
    def __init__(self, indent_level, contents):
        self.indent_level = indent_level
        self.contents = contents 
    
    def visit(self, local_scope):
        for elt in self.contents:
            elt.visit(local_scope)
    
    def run(self, call_frame):
        for elt in self.contents:
            elt.run(call_frame)

    def __repr__(self):
        return f"\n<Body indt:{self.indent_level}>{self.contents}\n</Body indt:{self.indent_level}>"

class Root(Body):
    def __init__(self, indent_level, contents): 
        self.indent_level = indent_level
        builtins = self.get_builtins()
        self.contents = builtins + contents

    def get_builtins(self):
        builtins = [BIPrint(), ]
        return builtins

    def visit(self, local_scope):
        for elt in self.contents:
            elt.visit(local_scope)
    
    def run(self, call_frame):
        for elt in self.contents:
            elt.run(call_frame)

    def __repr__(self):
        return f"\n<Root indt:{self.indent_level}>{self.contents}\n</Root indt:{self.indent_level}>"
