import analyzer

class NonValue(object):
    """same as in analyzer """
    pass

class CallFrame(object):
    def __init__(self, parent=None):
        self.symbol_table = dict() 
        self.return_value = None
        self.parent = parent

    def spawn_call_frame(self):
        # env_frame is the CallFrame from which the spawned frame is called
        new_call_frame = CallFrame(self)
        return new_call_frame

    def insert_symbol(self, name):
        if name in self.symbol_table:
            pass
        else:
            self.symbol_table[name] = NonValue()

    def lookup_symbol(self, name):
        try:
            value = self.symbol_table[name]
        except:
            return self.parent.lookup_symbol(name)
        # why does symbol_table.get raise an exception even when it doesn't
        # fall back to default value??
        # value = self.symbol_table.get(name, self.parent.lookup_symbol(name))
        return value

    def assign_symbol(self, name, value):
        # for now, values should be in production form
        # until evaluation
        self.symbol_table[name] = value
            

class Interpreter(object):
    def __init__(self, token_map, code):
        self.analyzer = analyzer.Analyzer(token_map, code)
        self.analyzer.analyze()
        self.scope = self.analyzer.scope
        self.ast = self.analyzer.ast
        # push the root scope onto the call stack
        self.stack = CallFrame() 

    def interpret(self):
        self.ast.run(self.stack)  
