#!/usr/bin/env python
import analyzer

TOKEN_MAP = [ 
        ("DEF", r"^def\s"),
        ("RETURN", r"^return\s*"),
        ("NAME", r"^[_A-Za-z]+[_0-9A-Za-z]*\s*"),
        ("LPAREN", r"^\(\s*"),
        ("INTEGER", r"^[0-9]+\s*"),
        ("COMMA", r"^,\s*"),
        ("RPAREN", r"^\)\s*"),
        ("COLON", r"^:\s*"),
        ("PLUS", r"^\+\s*"),
        ("ASSIGN", r"^\=\s*"),
        ("INDENT", r"^\s+"),
        ] 

CODE_SAMPLE = """def hello(x, y):
    print(x + y)

hello(1, 2)
hello(3, 4)
a = 5
b = 6 + 7
c = a + b
a + b + c
hello(a, b)
"""

def main():
    code = CODE_SAMPLE.split('\n')
    # print(">>>CODE:\n")
    for line in code:
        print(line)
    lyza = analyzer.Analyzer(TOKEN_MAP, code)    
    lyza.analyze()
    print(lyza.scope)

if __name__=="__main__":
    main()
