#!/usr/bin/env python3
accumulator = list()
def A(x):
  if x == 0:
    print("}}} base case, x == 0")
  else:
    def B():
      print(f"B(): A's x == {x}")
    accumulator.append(B)
    A(x-1)

A(4)
[elt() for elt in accumulator]
