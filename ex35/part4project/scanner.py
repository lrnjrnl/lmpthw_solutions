import re

class Token(object):
    def __init__(self, token_type, lexeme, start, indent_level, line_num):
        self.token_type = token_type
        self.lexeme = lexeme
        self.indent_level = indent_level
        self.start = start
        self.line_num = line_num
        
    def __repr__(self):
        ret = f"\n<{self.token_type} lex='{self.lexeme}'"
        ret += f" pos={self.start} indent={self.indent_level}"
        ret += f" linenum={self.line_num}>"
        return ret

class Scanner(object):
    def __init__(self, token_map):
        self.token_map = token_map
        self.tokenized = list() 
    
    def scan(self, script):
        output = list()
        line_num = 0
        for line in script:
            line_num += 1
            tokens = self.tokenize(self.token_map, line, line_num)
            if tokens:
                output.extend(tokens)
        self.tokenized.extend(output)

    def tokenize(self, token_map, line, line_num):
        tokens = list()
        linepos = 0
        while line:
            # linepos offsets the match.span values back to the original line
            indent_level = 0
            for token_type, pattern in token_map:
                t_match = None
                t_match = re.match(pattern, line)
                if t_match:
                    start, end = t_match.span() 
                    lexm = line[start:end]
                    line = line[end:]
                    start = start + linepos
                    token = Token(
                                token_type, lexm, start,
                                indent_level, line_num
                                )
                    tokens.append(token)
                    linepos = linepos + end
                    break
            assert t_match, f"INVALID TOKEN {pattern} AT {line}"
        if not tokens:
            return None
        else:
            self.clean_tokens(tokens)
            return tokens

    def clean_tokens(self, tokens):
        if tokens[0].token_type == 'INDENT':
            indent_level = len(tokens[0].lexeme)//4
            tokens.pop(0)
            for t in tokens:
                t.indent_level = indent_level
        for t in tokens:
            before = t.lexeme
            if t.token_type != 'INDENT':
                t.lexeme = t.lexeme.rstrip()
            after = t.lexeme
            # print(f">>>BEFORE: '{before}'\t AFTER: '{after}'")

    def peek(self):
        if self.tokenized:
            return self.tokenized[0].token_type
        else:
            return None

    def match(self, token_type):
        if token_type == self.peek():
            return self.tokenized.pop(0)
        else:
            return None

    def push(self, token):
        assert token, ">>>Scanner.push: empty token!"
        self.tokenized.insert(0, token)

    def pop(self):
        return self.tokenized.pop(0)

    def get_indent(self):
        if self.tokenized:
            return self.tokenized[0].indent_level
        else:
            return -1

    def get_line_num(self):
        if self.tokenized:
            return self.tokenized[0].line_num
        else:
            return -1
