from queue import *

def test_shift():
    train = Queue()
    assert train.count() == 0
    train.shift("engine")
    assert train.count() == 1
    train.shift("coal")
    assert train.count() == 2
    train.shift("caboose")
    assert train.count() == 3

def test_unshift():
    train = Queue()
    assert train.count() == 0
    train.shift("engine")
    assert train.count() == 1
    train.shift("coal")
    assert train.count() == 2
    train.shift("caboose")
    assert train.count() == 3
    
    assert train.unshift() == "engine"
    assert train.count() == 2
    assert train.unshift() == "coal"
    assert train.count() == 1
    assert train.unshift() == "caboose"
    assert train.count() == 0
    assert train.unshift() == None
    assert train.count() == 0
    
def test_frontback():
    train = Queue()
    assert train.count() == 0
    train.shift("engine")
    assert train.count() == 1
    train.shift("coal")
    assert train.count() == 2
    train.shift("caboose")
    assert train.count() == 3
    
    assert train.front() == "engine"
    assert train.back() == "caboose"
    assert train.unshift() == "engine"
    assert train.count() == 2
    assert train.front() == "coal"
    assert train.back() == "caboose"
    assert train.unshift() == "coal"
    assert train.count() == 1
    assert train.front() == "caboose"
    assert train.back() == "caboose"
    assert train.unshift() == "caboose"
    assert train.count() == 0
    assert train.front() == None
    assert train.back() == None
    assert train.unshift() == None
    assert train.count() == 0