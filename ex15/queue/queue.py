class QueueNode(object):
    def __init__(self, obj, nxt, prev):
        self.value = obj
        self.nxt = nxt
        self.prev = prev
        
    def __repr__(self):
        nval = self.nxt and self.nxt.value or None
        pval = self.prev and self.prev.value or None
        
        return f"[{self.value}:{repr(nval)}:{repr(pval)}]"
        
class Queue(object):
    
    def __init__(self):
        self.head = None
        self.tail = None
        
    def shift(self, obj):
        """appends an item to the tail of the queue."""
        self._invariant()
        shift_node = QueueNode(obj, None, self.tail)
        if self.head == None:
            self.head = shift_node
            self.tail = shift_node
        else:
            self.tail.nxt = shift_node
            self.tail = shift_node
        self._invariant()
            
    def unshift(self):
        """removes and returns an item from the head of the queue."""
        self._invariant()
        unshift_val = self.head and self.head.value or None
        if self.head != self.tail:
            if self.head.nxt:
                self.head = self.head.nxt
            self.head.prev = None
        else:
            self.head = None
            self.tail = None
        self._invariant()
        return unshift_val
    
    def front(self):
        """returns a reference to the item at the head of the queue."""
        return self.head and self.head.value or None
        
    def back(self):
        """returns a reference to the item at the tail of the queue."""
        return self.tail and self.tail.value or None
        
    def count(self):
        index = self.head
        count = 0
        while index:
            count += 1
            index = index.nxt
        return count
    
    def dump(self, mark="----"):
        pass
    
    def _invariant(self):
        if self.head:
            if self.tail != self.head:
                assert self.tail.prev and self.head.nxt
            assert  not (self.tail.nxt or self.head.prev)
        else:
            assert self.tail == self.head
