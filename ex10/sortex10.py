import argparse, sys

parser = argparse.ArgumentParser()
parser.add_argument('-o', nargs='?', action='store')
parser.add_argument('-r', action='store_true', default='store_false')
parser.add_argument('FILES', nargs='*', action='store')

args = parser.parse_args()

# NOTE: by default, the unix 'sort' command merges all the input files before performing the sort.
# get input from stdin pipe or input files, no newline stripping
# and store in unsorted list
unsorted = []
if not args.FILES:
    for line in sys.stdin:
        unsorted.append(line)
else:
    for file in args.FILES:
        with open(file) as fin:
            for line in fin.readlines():
                unsorted.append(line)


# dirty sort and output
sortout = sorted(unsorted, reverse=args.r)
if args.o:
    with open(args.o, 'w') as fout:
        for line in sortout:
            fout.write(line)
else:
    for line in sortout:
        print(line, end='')