from dllist import *

def bubble_sort(nums):
    """sort element values in ascending order"""
    # Performance: This is  a slow algorithm. There isn't much
    # that can be done here
    while True:
        ordered = True
        n = nums.begin
        n1 = n.nxt
        while n1:
            if n.value > n1.value:
                n.value, n1.value = n1.value, n.value
                ordered = False
            n = n1
            n1 = n1.nxt
        if ordered: break

def merge_sort(A):
    # Performance: this gets called a lot (recursive).
    #
    alength = A.count()
    # Base case. A list of zero or one elements is sorted, by definition.
    if alength <= 1:
        return A
        
    # Recursive case. First, divide the list into equal-sized sublists
    # consisting of the first half and second half of the list.
    # This assumes lists start at index 0.
    left = DoubleLinkedList()
    right = DoubleLinkedList()
    halfa = alength // 2
    index = 0
    while A.begin:
        if index < halfa:
            left.push(A.unshift())
        else:
            right.push(A.unshift())
        index +=1
    
    # Recursively sort both sides
    L = merge_sort(left)
    R = merge_sort(right)
    
    # Then merge the now-sorted sublists
    return merge(L, R)
    
    
def merge(left, right):
    result = DoubleLinkedList()
    
    while left.begin and right.begin:
        if left.first() <= right.first():
            result.push(left.unshift())
        else:
            result.push(right.unshift())
            
        # Either left or right may have elements left; consume them
        # print("left.begin", left.begin)
        # print("right.begin", right.begin)
        # (Only one of the following loops will actually be entered)
    while left.begin:
        result.push(left.unshift())
    while right.begin:
        result.push(right.unshift())
            
    return result
    
    
def refer_node(A, idx):
    """return node object which corresponds with index parameter"""
    # Performance: This gets called a hell of a lot.
    # The cumulative penalty is pretty bad. Try rebuilding the
    # count() method.
    acount = A.count()
    if idx > acount - 1:
        return None
    i = 0
    node = A.begin
    while i < idx:
        node = node.nxt
        i += 1
    return node
    

def asn_idx(A, idx, val):
    """assign val to idx node of list A"""
    acount = A.count()
    if idx > acount - 1:
        raise f"index out of range index: {idx} max:{acount - 1}"
    i = 0
    node = A.begin
    while i < idx:
        node = node.nxt
        i += 1
    node.value = val
    return
        
    
def quicksort(A, lo, hi):
    if lo < hi:
        p = partition(A, lo, hi)
        quicksort(A, lo, p - 1)
        quicksort(A, p + 1, hi)
        
        
def partition(A, lo, hi):
    pivot = A.get(hi)
    i = lo - 1
    j = lo
    inode = None
    while j <= hi - 1:
        jnode = refer_node(A, j)
        if jnode.value < pivot:
            i += 1
            inode = refer_node(A, i)
            jnode.value, inode.value = inode.value, jnode.value
        j += 1
    iplusnode = refer_node(A, i + 1)
    hinode = refer_node(A, hi)
    if hinode.value < iplusnode.value:
        hinode.value, iplusnode.value = iplusnode.value, hinode.value
    return i + 1
    
    
def qsort(A):
    quicksort(A, 0, A.count() - 1)