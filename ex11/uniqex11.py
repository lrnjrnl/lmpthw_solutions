import argparse, sys

# parse the arguments
parser = argparse.ArgumentParser()
parser.add_argument("-s", nargs="?", type=int, action="store")
parser.add_argument("FILES", nargs="*")

args = parser.parse_args()
print(args)


# I need to find out how to have sys.stdin override any FILES when getting a pipe from the commandline
def get_data():
    # collect lines of text from pipe or from file list
    in_data = []
    if not args.FILES:
        in_data = sys.stdin.readlines()
    elif args.FILES:
        for file in args.FILES:
            with open(file) as fin:
                in_data.extend([line for line in fin.readlines()])
    else:
        print("Need data source")
        exit(0)
    
    return in_data

def process_default(in_data):
    global args
    out_data = []
    last_line = ''
    for line in in_data:
        if line == last_line:
            continue
        out_data.append(line)
        last_line = line
        
    return out_data
    
def process_skip(in_data):
    # ignore matches before "args.s -1" characters
    global args
    out_data = []
    last_line = ''
    skippy = args.s - 1
    for line in in_data:
        if line[skippy:] == last_line[skippy:]:
            continue
        out_data.append(line)
        last_line = line
    
    return out_data
    

def processor(in_data):
    # this chooses which kind of processing to use depending on cli arguments
    global args
    if args.s:
        out_data = process_skip(in_data)
    else:
        # default
        out_data = process_default(in_data)
        
    for line in out_data:
        print(line, end='')
        
processor(get_data())