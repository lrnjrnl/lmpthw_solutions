#!/usr/bin/env python3

import argparse, glob, os, re

# parse command options
parser = argparse.ArgumentParser()
parser.add_argument('-r', action='store_true')
parser.add_argument('-f', action='store', nargs='?')
parser.add_argument('PATTERN', nargs='?', action='store')
parser.add_argument('FILES', type=str, nargs='*')

args = parser.parse_args()

print(args)

"""NAME
       grep, egrep, fgrep, rgrep - print lines matching a pattern

SYNOPSIS
       grep [OPTIONS] PATTERN [FILE...]
       grep [OPTIONS] -e PATTERN ... [FILE...]
       grep [OPTIONS] -f FILE ... [FILE...]

DESCRIPTION
       grep  searches  for  PATTERN in each FILE.  A FILE of "-" stands for standard input.  If no FILE is given, recursive searches
       examine the working directory, and nonrecursive searches read standard input.  By default, grep prints the matching lines.

       In addition, the variant programs egrep, fgrep and rgrep are the same as grep -E, grep -F, and grep -r, respectively.   These
       variants are deprecated, but are provided for backward compatibility."""
    
def files_glob():
    global args
    g = []
    h = []
    for f in args.FILES:
        if os.path.isfile(f):
            g.extend(glob.glob(f))
        elif os.path.isdir(f):
            # recurse through directories
            g.extend(glob.glob(f + '/**', recursive=args.r))
        else:
            print(f, " is not a file/directory!")
            exit(0)
            
    h = [elt for elt in g if os.path.isfile(elt)]
    return h

input_files = files_glob()

# test getting patterns from file(s)
# TODO: experiment with grep to see if pattern files can be globbed.


def get_patterns():
    patterns = []
    if os.path.isfile(args.f):
        with open(args.PATTERN) as f:
            for line in f.readlines():
                pattern.append(line)
    elif args.PATTERN:
        