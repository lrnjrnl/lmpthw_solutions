from dllist import *

class Dictionary(object):
    def __init__(self, num_buckets=256):
        self.num_buckets = num_buckets
        self.map = list()
        i = 0
        while i < num_buckets:
            self.map.append(list())
            i += 1
    
    def hash_key(self, key):
        return hash(key) % self.num_buckets
            
        
    def get_slot(self, key):
        bucket = self.get_bucket(key)
        
        if bucket:
            for slot in bucket:
                if slot[0] == key:
                    return bucket, slot
        # There should always be a bucket,
        # but just in case...
        return bucket, None
                
        
    def get_bucket(self, key):
        bucket_id = self.hash_key(key)
        return self.map[bucket_id]
        

    def set(self, key, value):
        bucket, slot = self.get_slot(key)
        
        if slot:
            slot = (key, value)
        else:
            bucket.append((key, value))
            
            
    def get(self, key):
        bucket, slot = self.get_slot(key)
        
        if slot:
            return slot[1]
        else:
            return None
        
    def delete(self, key):
        bucket, slot = self.get_slot(key)
        bucket.detach_node(slot)
    
    def list(self):
        bucket = self.map.begin
        for bucket in self.map:
            for slot in bucket:
                print(f"k: {slot[0]}, v: {slot[1]}")
                slot = slot.nxt

    
    
# TEST
if __name__ == '__main__':
    d = Dictionary(16)
    # print(d.get_bucket('abc'))
    d.set('blah', 'first key')
    d.set('blahblah', 'second key')
    d.set(1234, 'third key')
    # print('blah: ', d.get('blah'))
    # print('blahblah: ', d.get('blahblah'))
    # print(1234, d.get(1234))
    # print(12345, d.get(12345))
    # d.delete('blah')
    # print('blah: ', d.get('blah'))
    d.list()