import sys

hlp_msg = "Usage: python pypatch.py [original file] -i [diff file] -o [patched file]"


if len(sys.argv) != 6:
    print(hlp_msg)
    exit()
# this is a crummy hack to make the command line interface
# vaguely resemble that of patch
original_fn, patch_fn, output_fn = sys.argv[1], sys.argv[3], sys.argv[5]

orig = []
diff = []
output = ''

try:
    with open(original_fn) as f:
        orig = f.readlines()
    with open(patch_fn) as f:
        diff = f.readlines()
except:
    print("INVALID FILENAMES")

i = 0
opcode = []
while i < len(diff):
    line = diff[i]
    if line[0] == '<':
        pass
    elif line[0] == '>':
        output += line[2:]
    elif line[0] == 'e':
        opcode = line.strip().split(',')
        begin, end = int(opcode[1]), int(opcode[2])
        output += ''.join(orig[begin:end])
    i += 1

with open(output_fn, 'w') as f:
    f.write(output.strip())