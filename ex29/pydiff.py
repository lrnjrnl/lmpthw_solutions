import difflib
import sys

options = {'-c': False}

def process_args(sys_args):
    global options
    args = sys_args[1:]
    while args:
        if args[0] in options:
            options[arg] = True
            try:
                args = args[1:]
            except:
                print("USAGE: diffpatch -option [fileone] [filetwo]")
        else:
            try:
                filename_A, filename_B = args
                break
            except:
                print("Please supply two filenames")
    lines_A = []
    lines_B = []
    with open(filename_A) as f:
        lines_A = f.readlines()
    with open(filename_B) as f:
        lines_B = f.readlines()
    return lines_A, lines_B, options

def opequal(file_A, file_B, a1, a2, b1, b2):
    output = f"e,{a1},{a2},{b1},{b2}\n"
    return output

def opdelete(file_A, file_B, a1, a2, b1, b2):
    output = f"d,{a1},{a2},{b1},{b2}\n"
    output += ''.join(["< " + line for line in file_A[a1:a2]])
    return output

def opappend(file_A, file_B, a1, a2, b1, b2):
    output = f"a,{a1},{a2},{b1},{b2}\n"
    output += ''.join(["> " + line for line in file_B[b1:b2]])
    return output
    
def opinsert():
    pass

optags = {
        'equal': opequal,
        'delete': opdelete,
        'append': opappend,
        'insert': opappend
        }

def process_diff(opcodes, file_A, file_B):
    # create a diff from two files
    diff_out = ''
    for opcode in opcodes:
        tag_func = optags[opcode[0]]
        a1, a2, b1, b2 = opcode[1:]
        diff_out += tag_func(file_A, file_B, a1, a2, b1, b2)
    return diff_out
        
# def process_patch():
#     # apply a diff to the original file
#     patch_out = ''
#     return patch_out

#######

file1, file2, options = process_args(sys.argv)

matcher = difflib.SequenceMatcher(a=file1, b=file2)
opcodes = matcher.get_opcodes()
diff_out = process_diff(opcodes, file1, file2)
print(diff_out)
# patch_out = process_patch()