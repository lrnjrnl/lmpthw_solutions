import scanner
import productions

class ParserGrammarError(Exception):
    def __init__(self, grammar, tokens):
        mesg = f'Invalid token <{tokens[0]}> for "{grammar}" grammar'
        Exception.__init__(self, mesg)

class TokenMatchError(Exception):
    def __init__(self, label, tokens):
        mesg = f'Invalid label match, {label} in {tokens}'
        Exception.__init__(self, mesg)

class Parser(object):
    def __init__(self, patterns, mathin):
        self.scanner = scanner.Scanner(patterns, mathin)
        self.mathin = mathin
        self.tokens = self.scanner.tokens
        # table of operator precedence least-to-greatest
        # ASSIGN should have lower precedence, but because
        # it is right-associative, it needs higher precedence
        # for compatibility with the expression algo.
        self.assigns = "ASSIGN POWASGN DIVASGN PROASGN SUMASGN DIFASGN \
                ASSIGN".split(" ")
        self.build_op_prec()
        # table of operator productions
        self.operators = {
                "POWASGN":productions.PowAsgn,
                "QUOASGN":productions.QuoAsgn,
                "PROASGN":productions.ProAsgn,
                "SUMASGN":productions.SumAsgn,
                "DIFASGN":productions.DifAsgn,
                "ASSIGN":productions.Assign,
                "POW":productions.Pow,
                "QUO":productions.Quo,
                "PRO":productions.Pro,
                "SUM":productions.Sum,
                "NEG":productions.Dif,
                "EQTO":productions.EqTo,
                "GRTHAN":productions.GrThan,
                "LSTHAN":productions.LsThan,
                "LSEQTO":productions.LsEqTo,
                "GREQTO":productions.GrEqTo
                }
        self.ast = []
        self.current_line = 0
        self.parse()

    def build_op_prec(self):
        # these precedence levels come from the 
        # operator list in the gnu bc docs
        self.op_prec = { \
                        "NEG":9,
                        "POW":8,
                        "PRO":7,
                        "QUO":7,
                        "MOD":7,
                        "SUM":6,
                        "DIF":6,
                        "ASSIGN":5,
                        "SUMASGN":5,
                        "DIFASGN":5,
                        "PROASGN":5,
                        "QUOASGN":5,
                        "GRTHAN":4,
                        "LSTHAN":4,
                        "GREQTO":4,
                        "LSEQTO":4,
                        "EQTO":4
                        }

    def parse(self):
        while self.tokens:
            self.current_line = self.peekline()
            self.ast.append(self.statement_list()) 

    def peekline(self):
        return self.tokens and self.tokens[0].line_num
    
    def tokenpop(self):
        result = self.tokens and self.tokens.pop(0)
        self.current_line = result.line_num
        return result

    def tokenpush(self, token):
        self.tokens.insert(0, token)

    def peek(self):
        return self.tokens and self.tokens[0].label

    def match(self, label):
        if label == self.peek():
            return self.tokenpop() 
        else:
            raise TokenMatchError(label, self.tokens)

    def end_of_expression(self):
        next_label = self.peek()
        line_end = (self.current_line != self.peekline())
        semicolon_end = ("SEMICOLON" == next_label)
        paren_end = ("RPAREN"== next_label)
        if_end = ("ELSE" == next_label) 
        if line_end or semicolon_end or paren_end or if_end: 
            return True
        return False

    def has_precedence(self, prev_op, current_op):
        return self.op_prec[prev_op] >= self.op_prec[current_op]

    def get_term(self):
        next_label = self.peek()
        if "LPAREN" == next_label:
            return self.parentheses()
        elif "NEG" == next_label:
            return self.negation()
        elif "VARIABLE" == next_label:
            return self.variable()
        elif "NUMBER" == next_label:
            return self.number()
        else:
            return ParserGrammarError('expression.get_term', self.tokens)

    def get_op(self):
        # returns label string of operator
        op = self.peek()
        if op in self.operators.keys():
            self.match(op)
            return op
        else:
            raise ParserGrammarError("get_op", self.tokens)

    def op_node(self, LTm, LOp, RTm ):
        # Left Term, Left Operator, Right Term
        result =  self.operators[LOp](LTm, RTm)
        return result
    
    def statement_list(self):
        """ statement_list = statement (<SEMICOLON> statement)* """
        # gather all tokens from current line
        statements = []
        line_num = self.peekline()
        while line_num == self.peekline():
            statements.append(self.statement())
        return productions.StatementList(statements, line_num)

    def statement(self):
        """ <statement> ::= <expression> | <if_statement> | <while_statement>
        | <for_statement> """ 
        next_label = self.peek()
        current_line = self.peekline()
        if "IF" == next_label:
            return self.if_statement() 
        elif "WHILE" == next_label:
            return self.while_statement()
        elif "FOR" == next_label:
            return self.for_statement()
        else:
            return productions.Statement(self.expression())
    
    def expression(self):
        """ <expression> ::= <parentheses> | <multiplication>> | <addition>
        | <assignment> | <negation> | <exponent> | <number> | <variable> """
        expression = self.expr_recurse()
        return productions.Expression(expression)

    def assign_recurse(self, assign_op):
        LTm = self.get_term()        
        if self.end_of_expression():
            return LTm
        next_op = self.peek()
        if self.has_precedence(assign_op, next_op):
            # back out of recursion if assignment is followed
            # by lower-precedence operator 
            return LTm
        else:
            LOp = self.get_op()
            RTm = self.get_term()
        # parse rest of expression as usual    
        while not self.end_of_expression():
            ROp = self.peek()
            if self.has_precedence(LOp, ROp) and ROp not in self.assigns:
                LTm = self.op_node(LTm, LOp, RTm)
                if prev_term: 
                    return LTm
                else:
                    LOp = self.get_op()
                    RTm = self.get_term()
            else:
                if ROp in self.assigns:
                    ROp = self.get_op()
                    RHS_assign = self.assign_recurse(ROp)
                    RTm = self.op_node(RTm, ROp, RHS_assign)
                else:
                    RTm = self.expr_recurse(RTm)
        return self.op_node(LTm, LOp, RTm)

    def expr_recurse(self, prev_term=None):
        # grab the current operand production/node
        if prev_term:
            LTm = prev_term
        else:
            LTm = self.get_term()
        if self.end_of_expression():
            return LTm
        else:
            LOp = self.get_op()
            RTm = self.get_term()
        while not self.end_of_expression():
            ROp = self.peek()
            if self.has_precedence(LOp, ROp) and ROp not in self.assigns:
                # left operator has precedence
                LTm = self.op_node(LTm, LOp, RTm)
                if prev_term: 
                    return LTm
                else:
                    LOp = self.get_op()
                    RTm = self.get_term()
            else:
                # right operator has precedence, so
                # send right term into recursion to 
                # parse right side as larger expression
                if ROp in self.assigns:
                    # assigns evaluate RHS first as separate expression
                    # because of right-associativity
                    ROp = self.get_op()
                    # assignment needs to back out of recurse if next operator
                    # is lower precedence (ie relational). assign_recurse
                    # is expr_recurse wrapper for this purpose
                    # and contains the same expression parsing algo.
                    # This seems innefficient. Reconsider design.
                    RHS_assign = self.assign_recurse(ROp)
                    RTm = self.op_node(RTm, ROp, RHS_assign)
                else:
                    RTm = self.expr_recurse(RTm)
        return self.op_node(LTm, LOp, RTm)

    def if_statement(self):
        """ <if_statement> ::= IF LPAREN <expression> RPAREN <statement> [ELSE <statement>] """
        self.match('IF')
        self.match('LPAREN')
        test = self.expression()
        self.match('RPAREN')
        action_true = self.statement()
        action_false = None
        if "ELSE" == self.peek():
            self.match("ELSE")
            action_false = self.statement()
        return productions.IfStatement(test, action_true, action_false)

    def while_statement(self):
        """ <while_statement> ::= WHILE LPAREN <expression> RPAREN <statement> """
        self.match('WHILE')
        self.match('LPAREN')
        test = self.expression()
        self.match('RPAREN')
        action = self.statement()
        return productions.WhileStatement(test, action)

    def for_statement(self):
        """ <for_statement> ::= FOR LPAREN <expression> SEMICOLON <expression> \
                SEMICOLON <expression> RPAREN <statement> """
        self.match('FOR')
        self.match('LPAREN')
        start_expr = self.expression()
        self.match('SEMICOLON')
        test = self.expression()
        self.match('SEMICOLON')
        itr_expr = self.expression()
        self.match('RPAREN')
        action = self.statement()
        return productions.ForStatement(start_expr, test, itr_expr, action)

    def variable(self):
        result = self.match("VARIABLE")
        return productions.Variable(result.lexeme)

    def number(self):
        result = self.match("NUMBER")
        return productions.Number(result.lexeme)
            
    def negation(self):
        """ <negation> ::= NEG <expression> """
        self.match("NEG")
        if self.peek == "NEG":
            raise ParserGrammarError("negation", tokens)
        operand = self.get_term()
        return productions.Negation(operand)

    def parentheses(self):
        """ <parentheses> ::= LPAREN <expression> RPAREN """
        self.match("LPAREN")
        expression = self.expression() 
        self.match("RPAREN")
        return productions.Parentheses(expression)
