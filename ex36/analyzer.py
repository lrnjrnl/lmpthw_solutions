import parser

class Scope(object):
    def __init__(self):
        self.symbol_table = {}

    def __repr__(self):
        return f"{self.symbol_table}"

class Analyzer(object):
    def __init__(self, patterns, mathin):
        self.mathin = mathin
        self.parser = parser.Parser(patterns, mathin)
        self.ast = self.parser.ast
        self.scope = Scope()
        for elt in self.ast:
            elt.visit(self.scope) 
