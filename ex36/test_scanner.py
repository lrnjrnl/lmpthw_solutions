#!/usr/bin/env python3
import re
import scanner

PATTERNS = (
                ("^if\s*", "IF"),
                ("^else\s*", "ELSE"),
                ("^while\s*", "WHILE"),
                ("^for\s*", "FOR"),
                ("^\;\s*", "SEMICOLON"),
                ("^[A-Za-z]+\s*", "VARIABLE"),
                ("^(\=\=|\!\=|\>\=|\>|\<\=|\<)\s*", "RELATIONAL"),
                ("^[\^\*\/\+\-]\=\s*", "OPASSIGN"),
                ("^\=\s*", "ASSIGN"),
                ("^[0-9]+\.?[0-9]*\s*", "NUMBER"),
                ("^\.[0-9]+\s*", "NUMBER"),
                ("^\(\s*", "LPAREN"),
                ("^\)\s*", "RPAREN"),
                ("^[\^\*\/\+]\s*", "OPERATOR"),
                ("^\-\s*", "NEG")
)

MATH = """
x -= 2
-x + -2
"""

def main():
   scannie = scanner.Scanner(PATTERNS, MATH) 
   for token in scannie.tokens: print(token)

if __name__=="__main__":
    main()
