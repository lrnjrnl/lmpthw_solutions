import sys

def default_sub(arg1, arg2, instring):
    if not arg1 or not arg2:
        exit(0)
    i = 0
    while i < len(arg1):
        if i >= len(arg2):
            # reuse the last character from arg2
            instring = instring.replace(arg1[i], arg2[-1])

        else:
            instring = instring.replace(arg1[i], arg2[i])
        i += 1
                    
    return instring
    
def delete_chars(arg1, instring):
    broken_instring = list(instring)
    i = 0
    while i < len(broken_instring):
        if broken_instring[i] in arg1:
            del broken_instring[i]
        else:
            i += 1
    outstring = ''.join(broken_instring)
    return outstring
    
def main(args, instring):
    if args[1] == '-d':
        return delete_chars(args[2], instring)
    else:
        return default_sub(args[1], args[2], instring)
    
if __name__=="__main__":
    raw_in = sys.stdin.read()
    print(main(sys.argv, raw_in))