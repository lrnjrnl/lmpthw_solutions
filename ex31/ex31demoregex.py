import re

def test_one():
    ptrn = '.*BC?$'
    pattern_one = re.compile(ptrn)
    words = ('helloBC', 'helloB', 'helloA', 'helloBCX')
    print('>>> pattern one:')
    for word in words:
        if pattern_one.match(word):
            print(f"{word} is MATCH")
        else:
            print(f"{word} is FAIL")

def test_two():
    ptrn = '[A-Za-z][0-9]+'
    pattern_two = re.compile(ptrn)
    words = ('A1232344', 'abc1234', '12345', 'b493034')
    print('>>> pattern two :')
    for word in words:
        if pattern_two.match(word):
            print(f"{word} is MATCH")
        else:
            print(f"{word} is FAIL")

if __name__=="__main__":
    test_one()
    test_two()