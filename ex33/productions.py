class Production(object):
    pass

class Root(Production):
    def __init__(self, body):
        self.body = body
        
    def __repr__(self):
        return f"\n<ROOT>\n body: {self.body}\n</ROOT>"

class FuncDef(Production):
    def __init__(self, name_token, params, body):
        self.prod_type = "FUNCDEF"
        self.name = name_token.lexeme
        self.params = params
        self.body = body
        
    def __repr__(self):
        return f"\n<FUNCDEF>\n name: {self.name} \n params: {self.params} \n body: {self.body}\n</FUNCDEF>"
                
        
class Params(Production):
    def __init__(self, parameters):
        self.prod_type = "PARAMS"
        self.parameters = parameters
        
    def __repr__(self):
        return f"\n<PARAMS>\n parameters: {self.parameters}\n</PARAMS>"
        
class Body(Production):
    def __init__(self, contents, indent_level=0):
        self.prod_type = "BODY"
        self.indent_level = indent_level
        self.contents = contents
        
    def __repr__(self):
        return f"\n<BODY indent: {self.indent_level} >\n contents: {self.contents}\n</BODY>"

class ExprVariable(Production):
    def __init__(self, var_token):
        self.prod_type = "EXPRVAR"
        self.identifier = var_token.lexeme
        
    def __repr__(self):
        return f"\n<EXPRVAR>\n identifier: {self.identifier}\n</EXPRVAR>"
        
class ExprNumber(Production):
    def __init__(self, num_token):
        self.prod_type = "EXPRNUM"
        self.value = int(num_token.lexeme)
        
    def __repr__(self):
        return f"\n<EXPRNUM>\n value: {self.value}\n</EXPRNUM>"
        
class ExprPlus(Production):
    def __init__(self, left, right):
        self.prod_type = "EXPRPLUS"
        self.left = left
        self.right = right
    
    def __repr__(self):
        return f"\n<EXPRPLUS>\n left: {self.left}\n right: {self.right}\n</EXPRPLUS>"
        
class ExprFCall(Production):
    def __init__(self, function_prod):
        self.prod_type = "EXPRFCALL"
        self.function_call = function_prod
        
    def __repr__(self):
        return f"\n<EXPRFCALL>\n function_call: {self.function_call}\n</EXPRFCALL>"
        
class FuncCall(Production):
    def __init__(self, name_token, params):
        self.prod_type = "FUNCCALL"
        self.name = name_token.lexeme
        self.params = params
    
    def __repr__(self):
        return f"\n<FUNCCALL>\n name: {self.name}\n params: {self.params}\n</FUNCCALL>"