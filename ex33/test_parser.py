import parser
import scanner
import pprint


TOKENS = [
            (r"^def",                       "DEF"),
            (r"^[a-zA-Z_][a-zA-Z0-9_]*",    "NAME"),
            (r"^\(",                        "LPAREN"),
            (r"^[0-9]+",                    "INTEGER"),
            (r"^\+",                        "PLUS"),
            (r"^\)",                        "RPAREN"),
            (r"^:",                         "COLON"),
            (r"^\s\s\s\s",                  "INDENT"),
            (r"^\s+",                       "WHITESPACE"),
            (r"^,",                         "COMMA"),
            ]
            
            
CODE = """def hello(x, y):
    print(x + y)
    
hello(10, 20)

hello(4, 7)
"""

def test_Scanner():
    global TOKENS
    global CODE
    lilscan = scanner.Scanner(TOKENS, CODE)
    pprint.pprint(lilscan.tokens)

def test_Parser():
    global TOKENS
    global CODE
    # print(parser)
    lilparse = parser.Parser(TOKENS, CODE)
    print(lilparse.parse_tree)
    

        
if __name__=="__main__":
    # test_Scanner()
    test_Parser()